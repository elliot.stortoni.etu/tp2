const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image:
			'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300',
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image:
			'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300',
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image:
			'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	},
];
class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	render() {
		if (this.children == null || this.children == undefined) {
			if (this.attribute == null || this.attribute == undefined) {
				return null;
			} else {
				return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" />`;
			}
		} else {
			if (this.attribute == null || this.attribute == undefined) {
				return `<${this.tagName}>${this.children}</${this.tagName}`;
			} else {
				return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}">${this.children}</${this.tagName}>`;
			}
		}
	}
}
class Img extends Component {
	constructor(src) {
		super('img', { name: 'src', value: src });
	}
}
const title = new Component('h1', null, 'La Carte');
document.querySelector('.pageTitle').innerHTML = title.render();
const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);
document.querySelector('.pageContent').innerHTML = img.render();
